import requests

#from selenium import webdriver
#from selenium.webdriver.common.keys import Keys

from lxml import html

import json
import time
import random
import os
import sys


FAST = False
WAIT = 5 * 60
proxies_url = 'http://www.proxynova.com/proxy-server-list/elite-proxies/'

#page_url = 'http://www.keng.ru/fashion-marafon/u/999718/'
page_url = 'http://www.keng.ru/fashion-marafon/u/1003601/' #demi


# deprecated
#def vote():
#    page_url = 'http://www.keng.ru/fashion-marafon/u/999718/'
#
#    driver = webdriver.Firefox()
#    driver.get(page_url)
#
#    # hide add
#    #driver.find_element_by_class_name('close').click()
#
#    #elem = driver.find_element_by_class_name('b-konkurs-fashion-user-vote')
#    #elem.click()
#
#    cookies = driver.get_cookies()
#
#    # show cookies
#    for cookie in cookies:
#    #    if cookie.get('name') == 'BITRIX_SM_GEOIP':
#    #        geoip = cookie.get('value')
#    #    if cookie.get('name') == 'BITRIX_SM_LAST_IP':
#    #        last_ip = cookie.get('value')
#        print(cookie)
#
#
#    #driver.close()
#    #driver.quit()

def post_with_requests(proxy):
    # proxy is a dict {'http': 127.0.0.1:8000}
    # returns response as a json
    vote_url = 'http://www.keng.ru/fashion-marafon/ajax-vote.php'

    r = requests.post(vote_url, 
                    #data={'people_id': '999718'}, 
                    data={'people_id': '1003601'}, 
                    proxies=proxy,
                    timeout=10)
    return r.json()

def get_proxies(url):
    r = requests.get(url)    
    tree = html.fromstring(r.content)
    
    tbody = tree.xpath("//table[@id='tbl_proxy_list']/tbody")[0]
    
    result = {}
    for tr in tbody.getchildren():
        try:
            td1 = tr.getchildren()[0]
            td2 = tr.getchildren()[1]
            ip = td1.getchildren()[0].text + td1.getchildren()[1].text
            port = td2.getchildren()[0].text
            address = '{0}:{1}'.format(ip, port)
            result[address] = 0
        except:
            pass

    return result

def update_proxies_list(addresses):
    ''' 
    get dictionary of address: used
    update file only if used == False
    '''
    if os.path.exists('proxies.txt'):
        with open('proxies.txt', 'r+') as f:
            content = f.read()
            # if empty
            if not content:
                content = '{}'
            else:
                f.seek(0)
                f.truncate()
    else:
        # create file, if not exists
        with open('proxies.txt', 'w') as f:
            content = '{}'

    content = json.loads(content)
    for k, v in addresses.items():
        if not v:
            content[k] = v

    with open('proxies.txt', 'w') as f:
        f.write(json.dumps(content))

    return


#def vote():
#    with open('proxies.txt') as f:
#        content = f.read()
#
#    if content: 
#        content = json.loads(content)
#    else: 
#        exit('proxies.txt is empty')
#    for k, v in content.items():
#        if v == False:
#
#            try:
#                response = post_with_requests({'http': k})
#                if response.get('status') == 'ok':
#                    content[k] = 'ok' 
#                    print('OK {}'.format(k))
#                else: 
#                    content[k] = 'fail'
#                    print('FAIL {}'.format(k))
#            except:
#                content[k] = 'exception' 
#                print('EXCEPTION {}'.format(k))
#                pass
#
#            #try:
#            #    post_with_requests({'http': k})
#            #    content[k] = 1
#            #except:
#            #    pass
#
#            #time.sleep(WAIT)
#
#            with open('proxies.txt', 'w+') as f:
#                f.write(json.dumps(content))
#
#            if FAST:
#                pass
#            else:
#                time_to_sleep = random.randint(30, 60*5)
#                print('sleep for {0}s'.format(time_to_sleep))
#                time.sleep(time_to_sleep)
#
#            # vote only once
#            break
#
#    return


def vote2(k):
    with open('proxies.txt', 'r+') as f:
        content = json.loads(f.read())
        f.seek(0)
        f.truncate()
        try:
            response = post_with_requests({'http': k})
            if response.get('status') == 'ok':
                content[k] = 'ok' 
                print('OK {}'.format(k))
            else: 
                content[k] = 'fail'
                print('FAIL {}'.format(k))
        except:
            content[k] = 'exception' 
            print('EXCEPTION {}'.format(k))
        f.write(json.dumps(content))

    if FAST:
        pass
    else:
        time_to_sleep = random.randint(10, 20)
        print('sleep for {0}s'.format(time_to_sleep))
        time.sleep(time_to_sleep)

    return


def get_votes_count():
    r = requests.get(page_url)
    tree = html.fromstring(r.content)
    votes = tree.xpath("//div[@class='b-konkurs-fashion-user-vote']/span/text()")[0]
    return int(votes)


def get_vacant_proxy():
    with open('proxies.txt', 'r') as f:
        content = f.read()
        if not content:
            content = '{}'
        content = json.loads(content)
        for k, v in content.items():
            if not v:
                return k
        return
        

def main(n):
    while True:
        votes = get_votes_count()
        vacant_proxy = get_vacant_proxy()
        if vacant_proxy is None:
            addresses = get_proxies(proxies_url)
            update_proxies_list(addresses)
            vacant_proxy = get_vacant_proxy()
        if votes < n:
            vote2(vacant_proxy)
        else: 
            print('got {0} votes'.format(votes))
            break

#main(600)

#if __name__ == '__main__':
#
#    args = sys.argv[1:]
#    if args[0] == '--proxy':
#        r = post_with_requests({'http': args[1]})
#        print(r)
