#with open('proxies_big_list.txt') as f:

with open('p1.txt') as f:
    p1 = f.read().split()

with open('p2.txt') as f:
    p2 = f.read().split()

res = set(p1) - set(p2)

with open('4_08/proxies_result.txt', 'w') as f:
    f.write('\n'.join(res))
