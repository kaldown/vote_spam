from lxml import html
import requests
import time
import random

PAGE_URL = 'http://www.keng.ru/fashion-marafon/u/1003601/' #demi

#with open('proxies_big_list.txt', 'r+') as f:
#    for line in f:
#        try:
#            r = post_with_requests({'http': line.strip()})
#            print(r)
#            time.sleep(5)
#        except:
#            pass

def post_with_requests(proxy):
    # proxy is a dict {'http': 127.0.0.1:8000}
    # returns response as a json
    vote_url = 'http://www.keng.ru/fashion-marafon/ajax-vote.php'

    r = requests.post(vote_url, 
                    data={'people_id': '1003601'}, 
                    proxies={'http': ''.join(['http://', proxy])},
                    timeout=10)
    return r.json()

def get_votes_count(page_url):
    r = requests.get(page_url)
    tree = html.fromstring(r.content)
    votes = tree.xpath("//div[contains(@class, 'b-konkurs-fashion-user-vote')]/span/text()")[0]
    return int(votes)

def main(numb):
    with open('proxies_result.txt') as f:
        count_success = 0
        for i, proxy in enumerate(f):
            try:
                votes = get_votes_count(PAGE_URL)
                if votes < numb:
                    response = post_with_requests(proxy.strip())
                    print(response)
                    sleep_time = random.randint(5, 30)
                    time.sleep(sleep_time)
                    if response.get('status') == 'ok':
                        print('succeed with {0}'.format(proxy))
                        count_success += 1
                        print(count_success)
                else: 
                    print('Done')
                    break
            except KeyboardInterrupt:
                exit(0)
            except:
                print('ERROR')
    print('got {0} votes, succeed = {1}'.format(votes, count_success))
    print('STOPPED at {0}'.format(i))

main(1600)

