#with open('proxies_big_list.txt') as f:

with open('p1.txt') as f:
    p1 = f.read().split()

with open('p2.txt') as f:
    p2 = f.read().split()

with open('p3.txt') as f:
    p3 = f.read().split()

res = set(p1 + p2 + p3)

with open('proxies_result.txt', 'w') as f:
    f.write('\n'.join(res))
